## This repository contains CAD design for 3D printed powder coating gun
Use [OpenScad](www.openscad.org/) to compile end export STL models for printing. Modify variable values, described in source, if you want to change something. 

Follow [this article](https://lordovervolt.com/powder-coating) for building tips.

Besides printed parts you also need:

- any small PC bottle, that can fit cap thread
- air blowing gun
- 16 mm PVC (PTFE is better) pipe tube (about 140 mm)
- 8 mm internal dia flexible tube
- 2 mm^2 copper wire, with one end sharpen
- HV cable or **high quality** 75 ohm concentric cable
- 15-25 kV DC supply unit 
