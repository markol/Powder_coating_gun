
translate([14,0,0]) wire_support();
translate([28,0,0]) wire_support();
cone();

module cone(hole_r = 1){
	rotate_extrude($fn=30){
		translate([hole_r,0,0])
		difference(){
			polygon([[0,0],[0,10],[12,0]]);

			translate([13,11,0])
			circle(r=13);
		}
	}
}

module wire_support(h = 15)
{
difference()
{
	cylinder (r=2, h = h);
	cylinder (r=1.2, h = h, $fn = 8);
}
difference()
{
	cylinder (r=6, h = h);
	cylinder (r=5.5, h = h);
}
	for(i = [0:120:360])
	{
		rotate([0,0,-i]) translate([0,3.5,h/2]) cube([1,4,h], center= true);
	}
}